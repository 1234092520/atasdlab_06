﻿using System;
using System.Diagnostics;

class Program
{
    static void Main()
    {
        int[] sizes = { 10, 100, 500, 1000, 2000, 5000, 10000 };

        foreach (int size in sizes)
        {
            int[] array = GenerateIntArray(size);

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            HeapSort(array);

            stopwatch.Stop();

            
            long elapsedTicks = stopwatch.ElapsedTicks;
            long elapsedNanoseconds = (elapsedTicks * 1_000_000_000) / Stopwatch.Frequency;

            Console.WriteLine($"Сортування масиву розміром {size} зайняло {elapsedNanoseconds} наносекунд.");
        }
    }

    static int[] GenerateIntArray(int size)
    {
        Random random = new Random();
        int[] array = new int[size];
        for (int i = 0; i < size; i++)
        {
            array[i] = random.Next(0, 301);
        }
        return array;
    }

    static void HeapSort(int[] array)
    {
        int n = array.Length;

        for (int i = n / 2 - 1; i >= 0; i--)
        {
            Heapify(array, n, i);
        }

        for (int i = n - 1; i > 0; i--)
        {
            int temp = array[0];
            array[0] = array[i];
            array[i] = temp;

            Heapify(array, i, 0);
        }
    }

    static void Heapify(int[] array, int n, int i)
    {
        int largest = i;
        int left = 2 * i + 1;
        int right = 2 * i + 2;

        if (left < n && array[left] > array[largest])
        {
            largest = left;
        }

        if (right < n && array[right] > array[largest])
        {
            largest = right;
        }

        if (largest != i)
        {
            int swap = array[i];
            array[i] = array[largest];
            array[largest] = swap;

            Heapify(array, n, largest);
        }
    }

    static void PrintArray(int[] array)
    {
        foreach (int num in array)
        {
            Console.Write(num + " ");
        }
        Console.WriteLine();
    }
}
