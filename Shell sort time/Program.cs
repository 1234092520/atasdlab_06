﻿using System;
using System.Diagnostics;

class Program
{
    static void Main()
    {
        int[] sizes = { 10, 100, 500, 1000, 2000, 5000, 10000 };

        foreach (int size in sizes)
        {
            float[] array = GenerateFloatArray(size, -100f, 10f);

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            ShellSort(array);

            stopwatch.Stop();

            long elapsedTicks = stopwatch.ElapsedTicks;
            long elapsedNanoseconds = (elapsedTicks * 1_000_000_000) / Stopwatch.Frequency;

            Console.WriteLine($"Сортування масиву розміром {size} зайняло {elapsedNanoseconds} наносекунд.");
        }
    }

    static float[] GenerateFloatArray(int size, float lower, float upper)
    {
        Random random = new Random();
        float[] array = new float[size];
        for (int i = 0; i < size; i++)
        {
            array[i] = (float)(random.NextDouble() * (upper - lower) + lower);
        }
        return array;
    }

    static void ShellSort(float[] arr)
    {
        int n = arr.Length;
        for (int gap = n / 2; gap > 0; gap /= 2)
        {
            for (int i = gap; i < n; i++)
            {
                float temp = arr[i];
                int j;
                for (j = i; j >= gap && arr[j - gap] > temp; j -= gap)
                {
                    arr[j] = arr[j - gap];
                }
                arr[j] = temp;
            }
        }
    }

    static void PrintArray(float[] arr)
    {
        foreach (float num in arr)
        {
            Console.Write(num + " ");
        }
        Console.WriteLine();
    }
}
