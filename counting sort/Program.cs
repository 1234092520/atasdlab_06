﻿using System;

class CountingSort
{
    public static void CountingSortMethod(int[] array)
    {
        int minVal = array[0];
        int maxVal = array[0];

        foreach (var num in array)
        {
            if (num < minVal) minVal = num;
            if (num > maxVal) maxVal = num;
        }

        int range = maxVal - minVal + 1;
        int[] count = new int[range];
        int[] output = new int[array.Length];

        foreach (var num in array)
        {
            count[num - minVal]++;
        }

        for (int i = 1; i < range; i++)
        {
            count[i] += count[i - 1];
        }

        for (int i = array.Length - 1; i >= 0; i--)
        {
            output[count[array[i] - minVal] - 1] = array[i];
            count[array[i] - minVal]--;
        }

        for (int i = 0; i < array.Length; i++)
        {
            array[i] = output[i];
        }
    }

    public static void Main()
    {
        Console.Write("Введіть кількість елементів: ");
        int numElements;
        while (!int.TryParse(Console.ReadLine(), out numElements) || numElements <= 0)
        {
            Console.Write("Неправильні дані. Будь ласка, введіть додатне ціле число: ");
        }

        Random random = new Random();
        int[] arr = new int[numElements];
        for (int i = 0; i < arr.Length; i++)
        {
            arr[i] = random.Next(-15, -9); 
        }

        Console.WriteLine("Згенерований масив: " + string.Join(", ", arr));
        CountingSortMethod(arr);
        Console.WriteLine("Відсортований масив: " + string.Join(", ", arr));
    }
}
