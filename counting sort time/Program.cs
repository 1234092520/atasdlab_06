﻿using System;
using System.Diagnostics;

class CountingSort
{
    public static void CountingSortMethod(int[] array)
    {
        int minVal = array[0];
        int maxVal = array[0];

        foreach (var num in array)
        {
            if (num < minVal) minVal = num;
            if (num > maxVal) maxVal = num;
        }

        int range = maxVal - minVal + 1;
        int[] count = new int[range];
        int[] output = new int[array.Length];

        foreach (var num in array)
        {
            count[num - minVal]++;
        }

        for (int i = 1; i < range; i++)
        {
            count[i] += count[i - 1];
        }

        for (int i = array.Length - 1; i >= 0; i--)
        {
            output[count[array[i] - minVal] - 1] = array[i];
            count[array[i] - minVal]--;
        }

        for (int i = 0; i < array.Length; i++)
        {
            array[i] = output[i];
        }
    }

    public static void Main()
    {
        int[] sizes = { 10, 100, 500, 1000, 2000, 5000, 10000 };

        foreach (int size in sizes)
        {
            int[] arr = GenerateIntArray(size);

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            CountingSortMethod(arr);

            stopwatch.Stop();

            long elapsedTicks = stopwatch.ElapsedTicks;
            long elapsedNanoseconds = (elapsedTicks * 1_000_000_000) / Stopwatch.Frequency;

            Console.WriteLine($"Сортування масиву розміром {size} зайняло {elapsedNanoseconds} наносекунд.");
        }
    }

    private static int[] GenerateIntArray(int size)
    {
        Random random = new Random();
        int[] arr = new int[size];
        for (int i = 0; i < size; i++)
        {
            arr[i] = random.Next(-15, 16); // Діапазон для значень
        }
        return arr;
    }
}
