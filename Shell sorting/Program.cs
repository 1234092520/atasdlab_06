﻿using System;

class Program
{
    static void Main()
    {
        Console.WriteLine("Введіть кількість елементів для сортування Шелла:");
        int size = int.Parse(Console.ReadLine());

        float[] arr = GenerateFloatArray(size, -100f, 10f);

        Console.WriteLine("Згенерований масив:");
        PrintArray(arr);

        ShellSort(arr);

        Console.WriteLine("Відсортований масив:");
        PrintArray(arr);
    }

    static float[] GenerateFloatArray(int size, float lower, float upper)
    {
        Random random = new Random();
        float[] arr = new float[size];
        for (int i = 0; i < size; i++)
        {
            float value;
            while (true)
            {
                value = (float)(random.NextDouble() * (upper - lower) + lower);
                if (value >= -100f && value <= 10f)
                {
                    break;
                }
                else
                {
                    Console.WriteLine($"Помилка: згенероване значення {value} виходить за межі діапазону [-100, 10]. Генеруємо нове значення...");
                }
            }
            arr[i] = value;
        }
        return arr;
    }

    static void ShellSort(float[] arr)
    {
        int n = arr.Length;
        for (int gap = n / 2; gap > 0; gap /= 2)
        {
            for (int i = gap; i < n; i++)
            {
                float temp = arr[i];
                int j;
                for (j = i; j >= gap && arr[j - gap] > temp; j -= gap)
                {
                    arr[j] = arr[j - gap];
                }
                arr[j] = temp;
            }
        }
    }

    static void PrintArray(float[] arr)
    {
        foreach (float num in arr)
        {
            Console.Write(num + " ");
        }
        Console.WriteLine();
    }
}
